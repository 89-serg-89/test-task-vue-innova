import Vue from 'vue'
import Vuex from 'vuex'
import { searchUpdate } from '~/utils/search'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        comments: []
    },
    getters: {
        comments: state => state.comments
    },
    mutations: {
        setComments(state, payload) {
            state.comments = payload
        },
        update(state, payload) {
            const comments = [...state.comments]
            state.comments = searchUpdate({...payload, comments})
        }
    },
    actions: {

    }
})

export default store