export function modify(data, level = 0) {
    ++level
    return data.map((item, index) => {
        item.count = 0
        item.level = level
        item.open = level > 2 ? false : true
        if(item.children) modify(item.children, level)
        return item
    })
}