export function searchUpdate(data) {
    let check = false

    const updateData = data.comments.map(i => {
        if(i._id === data._id) {
            check = true
            if(data.key === 'children') {
                i.children.push(data.value)
            } else {
                i[data.key] = data.value
            }
        }
        if(i.children && !check) searchUpdate({key: data.key, value: data.value, _id: data._id, comments: i.children})
        return i
    })

    return updateData
}